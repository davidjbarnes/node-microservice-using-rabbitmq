module.exports = function () {
    this.add("role:user,cmd:get", get);

    function get(req, done) {
        return done(null, { result: "User-".concat(Math.random()), when: Date.now() });
    };
};