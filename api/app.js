const express = require("express");
const join = require("bluebird").join;

const adderService = require("./services/adder");
const userService = require("./services/user");

const app = express();

app.get("/", function (req, res) {
    join(
        userService.get(),
        // adderService.sum(req.query.number1, req.query.number2),
        // adderService.difference(req.query.number1, req.query.number2),
        function (user) {
            res.send("user: ".concat(user.result));
        }
    );
});

module.exports = app;