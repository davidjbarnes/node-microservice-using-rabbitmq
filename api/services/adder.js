const client = require("seneca")()
    .use("seneca-amqp-transport")
    .client({
        type: "amqp",
        pin: "role:adder,cmd:*",
        url: "amqp://itKFic0X:GpgyM6s7yizu8bn77ZZFJabp0Mn0xTET@brown-kingcup-357.bigwig.lshift.net:10224/_8M1QgJ3n0Ju"
    });
    
const sum = (number1, number2) => {
    return new Promise(function(resolve, reject) {
        client.act("role:adder,cmd:add", {
            number1:number1,
            number2:number2
        }, (err, res) => {
            resolve(res);
        });
    });
};

const difference = (number1, number2) => {
    return new Promise(function(resolve, reject) {
        client.act("role:adder,cmd:difference", {
            number1:number1,
            number2:number2
        }, (err, res) => {
            resolve(res);
        });
    });
};

module.exports = {
    sum,
    difference
};