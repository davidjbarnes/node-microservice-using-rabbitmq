const client = require("seneca")()
.use("seneca-amqp-transport")
.client({
    type: "amqp",
    pin: "role:user,cmd:*",
    url: "amqp://itKFic0X:GpgyM6s7yizu8bn77ZZFJabp0Mn0xTET@brown-kingcup-357.bigwig.lshift.net:10224/_8M1QgJ3n0Ju"
});

const get = () => {
    return new Promise(function(resolve, reject) {
        client.act("role:user,cmd:get", {}, (err, res) => {
            resolve(res);
        });
    });
};

module.exports = {
    get
};