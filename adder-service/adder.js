module.exports = function () {
    this.add("role:adder,cmd:add", add);
    this.add("role:adder,cmd:difference", difference);
    this.add("role:adder,cmd:*", adder);

    function add(req, done) {
        var result = parseInt(req.number1) + parseInt(req.number2);
        return done(null, { result: result, when: Date.now() });
    };

    function difference(req, done) {
        var result = parseInt(req.number1) - parseInt(req.number2);
        return done(null, { result: result, when: Date.now() });
    };

    function adder(req, done) {
        return done(null, { result: "Hello from Adder-Service", when: Date.now() });
    };
};